// ==UserScript==
// @name         一键领取淘宝天猫优惠券
// @namespace    https://www.aizheteng.net/
// @version      1.0
// @description  操作简单，接口每日更新
// @author       shanelee
// @match        *://item.taobao.com/*
// @match        *://detail.tmall.com/*
// @require      http://cdn.bootcss.com/jquery/1.8.3/jquery.min.js
// @run-at       document-end
// @grant        unsafeWindow
// ==/UserScript==

(function() {
    $(document).ready(function() {
		var url       = window.location.host;
		var links = document.getElementsByTagName("link");
		var link = {};
        var str_host_name  = 'taobao';
        var str_goods_name = '';//$(document).attr('title');
        if(url.indexOf('taobao.com')==-1) str_host_name = 'tmall';
        if(str_host_name=='taobao'){
            str_goods_name = $('.tb-main-title').text();
        }else{
            str_goods_name = $('meta[name=keywords]').attr('content');

        }
        //str_goods_name=$.trim(str_goods_name);
        var itemName=$.trim(str_goods_name);     //str_goods_name.substring(0,15);
        //alert(str_goods_name);//打印商品名
        for(var i=0;i<links.length;i++){
			link = links[i];
			if(link.rel === "canonical"){
				itemURL = link.href;
				break;
				}
			}
			var itemId=itemURL.split("=")[1];
        	//alert(url); //打印商品id

			var btn_quan_taobao = '<a style="display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: normal;height:26px;line-height:26px;text-align: center;white-space: nowrap;vertical-align: middle;-ms-touch-action: manipulation;touch-action: manipulation;cursor: pointer;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;background-image: none;border: 1px solid transparent;border-radius:2px;color: #fff;background-color: #DF231C;#FF0036;margin-left:10px" href="https://tb.jrtjt.cn/api/tbq?itmename='+ encodeURI(itemName) +'&id='+ encodeURI(itemId) +'" target="_blank">获取优惠券</a>';
			var btn_quan_tmall =   '<div class="tb-action" style="margin-top:0"><a style="display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: normal;height:26px;line-height:26px;width:156px;text-align: center;white-space: nowrap;vertical-align: middle;-ms-touch-action: manipulation;touch-action: manipulation;cursor: pointer;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;background-image: none;border: 1px solid transparent;border-radius:2px;color: #fff;background-color: #DF231C;#FF0036;" href="https://tb.jrtjt.cn/api/tbq?itmename='+ encodeURI(itemName) +'&id='+ encodeURI(itemId) +'">获取优惠券</a></div>';
			var faNode = document.querySelector("div#J_Title p.tb-subtitle, div.tb-detail-hd");
			if(url.indexOf('taobao.com') > 0) {
				$('.tb-action').append(btn_quan_taobao);
				$('.tb-action').append(btn_search_taobao);

			}else{
				$('.tb-sku').append(btn_quan_tmall);
				$('.tb-sku').append(btn_search_tmall);
				}
			});
})();